# Templates by Wiki

For top 100 Wikis:
- total number of templates 
- number of templates that have been used at least five times
- top templates by number of transclusions in mainspace
- top templates by number of transclusions in non-mainspace

Part of [T357442](https://phabricator.wikimedia.org/T357442)
