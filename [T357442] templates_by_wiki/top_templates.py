import argparse
import pandas as pd
import warnings
from tqdm import tqdm
import wmfdata.mariadb as mariadb

def get_top_templates(wiki, main_ns=0):
    query = f"""
    WITH 
        base AS (
            SELECT 
                tl.tl_target_id,
                p.page_namespace,
                lt.lt_title,
                CASE
                    WHEN p.page_namespace = {main_ns} THEN 1
                    ELSE 0
                END AS is_mainspace
            FROM
                templatelinks tl
            JOIN
                page p ON tl.tl_from = p.page_id
            JOIN
                linktarget lt ON tl.tl_target_id = lt.lt_id
            WHERE
                NOT p.page_is_redirect
                AND lt.lt_namespace = 10
        ),
        
        agg AS (
            SELECT
                lt_title,
                is_mainspace,
                COUNT(*) AS count
            FROM
                base
            GROUP BY
                lt_title,
                is_mainspace
        ),
        
        ranking AS (
            SELECT
                lt_title,
                is_mainspace,
                count,
                ROW_NUMBER() OVER (
                    PARTITION BY is_mainspace
                    ORDER BY count DESC
                ) rn
            FROM
                agg
            WHERE
                lt_title NOT LIKE '%.css%'
        )
        
    SELECT 
        lt_title,
        is_mainspace
    FROM
        ranking
    WHERE
        rn <=10
    """
    result = mariadb.run(query, wiki)
    mainspace = result.query("is_mainspace == 1").lt_title.tolist()
    non_mainspace = result.query("is_mainspace == 0").lt_title.tolist()
    return {'main_ns': mainspace, 'non_main_ns': non_mainspace}

def process_wiki_and_update(wiki, main_ns, wikis):
    
    top_templates = get_top_templates(wiki, main_ns)
    wikis.loc[wikis['database_code'] == wiki, 'top_templates_main_ns'] = str(top_templates['main_ns'])
    wikis.loc[wikis['database_code'] == wiki, 'top_templates_non_main_ns'] = str(top_templates['non_main_ns'])

    wikis.to_csv('updated_wikis_with_top_templates.tsv', sep='\t', index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Retrieve top templates for wikis.")
    parser.add_argument("input_file", help="Path to the input TSV file with wiki database codes.")
    
    args = parser.parse_args()

    warnings.filterwarnings('ignore')
    
    wikis = pd.read_csv(args.input_file, sep='\t')
    db_codes = wikis['database_code'].values.tolist()[::-1]
    
    for wiki in tqdm(db_codes):
        main_ns = 6 if wiki == 'commonswiki' else 0
        process_wiki_and_update(wiki, main_ns, wikis)
