import pandas as pd
import warnings
from tqdm import tqdm
import wmfdata as wmf

def get_total_templates(wiki):
    query = """
        WITH 
            template_usage AS (
                SELECT 
                    tl.tl_target_id,
                    COUNT(*) AS usage_count
                FROM
                    templatelinks tl
                JOIN
                    page p ON tl.tl_from = p.page_id
                JOIN
                    linktarget lt ON tl.tl_target_id = lt.lt_id
                WHERE
                    p.page_is_redirect = 0
                    AND lt.lt_namespace = 10
                GROUP BY
                    tl.tl_target_id
            ),
            total_templates AS (
                SELECT
                    COUNT(DISTINCT page_id) AS total
                FROM
                    page
                WHERE
                    page_namespace = 10
                    AND NOT page_is_redirect
            ),
            templates_min5 AS (
                SELECT
                    COUNT(*) AS min5
                FROM
                    template_usage
                WHERE
                    usage_count >= 5
            )

        SELECT
            (SELECT total FROM total_templates) AS total_templates,
            (SELECT min5 FROM templates_min5) AS templates_min5
    """
    result = wmf.mariadb.run(query, wiki)
    return result.iloc[0].to_dict()

def process_wiki(wiki):
    template_counts = get_total_templates(wiki)
    wikis.loc[wikis['database_code'] == wiki, 'n_templates'] = template_counts['total_templates']
    wikis.loc[wikis['database_code'] == wiki, 'n_templates_min5'] = template_counts['templates_min5']
    
    wikis.to_csv('templates_usage_top100_wikis.tsv', index=False, sep='\t')

if __name__ == "__main__":
    warnings.filterwarnings('ignore')
    
    wikis = pd.read_csv('https://raw.githubusercontent.com/wikimedia-research/wiki-comparison/main/data-collection/snapshots/Jan_2023.tsv', sep='\t').iloc[:100, -7:]
    wikis.columns = [col.replace(' ', '_') for col in wikis.columns.values]
    db_codes = wikis.database_code.values.tolist()[::-1]

    for wiki in tqdm(db_codes):
        process_wiki(wiki)